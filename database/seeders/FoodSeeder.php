<?php

namespace Database\Seeders;

use App\Models\Food;
use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $foods = [
            [
                "title" => "Ham and Cheese Toastie",
                "ingredients" => [
                    "Ham",
                    "Cheese",
                    "Bread",
                    "Butter"
                ]
            ],
            [
                "title" => "Fry-up",
                "ingredients" => [
                    "Bacon",
                    "Eggs",
                    "Baked Beans",
                    "Mushrooms",
                    "Sausage",
                    "Bread"
                ]
            ],
            [
                "title" => "Salad",
                "ingredients" => [
                    "Lettuce",
                    "Tomato",
                    "Cucumber",
                    "Beetroot",
                    "Salad Dressing"
                ]
            ],
            [
                "title" => "Hotdog",
                "ingredients" => [
                    "Hotdog Bun",
                    "Sausage",
                    "Ketchup",
                    "Mustard"
                ]
            ]
        ];

        foreach ($foods as $item){
            $food = Food::query()->create(['title' => $item['title']]);
            $ingredientIds = Ingredient::query()->whereIn('title', $item['ingredients'])->get()->pluck('id');

            $food->ingredients()->sync($ingredientIds);
        }
    }
}
