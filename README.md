<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Execution Steps

The project implementation steps are in the following steps:

- **Set .env value**
- **Call command:** php artisan optimize
- **Call command:** php artisan migrate
- **Call command:** php artisan db:seed
- **Call command:** php artisan db:seed UserSeeder
- **Call command:** php artisan db:seed IngredientsSeeder
- **Call command:** php artisan db:seed FoodSeeder
- **Call command:** php artisan schedule:work         ------> Run cron job every minute

## Login User Password

- **Email:** test@test.com
- **Password:** Test@1234
