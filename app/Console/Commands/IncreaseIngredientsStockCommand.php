<?php

namespace App\Console\Commands;

use App\Models\Ingredient;
use Carbon\Carbon;
use Illuminate\Console\Command;

class IncreaseIngredientsStockCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'increase:stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Increase ingredients stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Ingredient::query()->whereStock(0)->update([
            'stock' => 4
        ]);
        return $this->info('Updated stock');
    }
}
