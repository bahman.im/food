<?php

namespace App\Models;

use App\Interfaces\Order as OrderContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'total_cost', 'is_paid', 'is_resolved', 'payment_driver'];

    /**
     * Relations
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }

    public function food()
    {
        return $this->foods()->first();
    }
}
