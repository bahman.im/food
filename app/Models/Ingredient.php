<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'stock', 'best_before', 'expires_at'];

    /**
     * Relations
     */
    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }
}
