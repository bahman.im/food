<?php

namespace App\Http\Controllers;

use App\Http\Requests\Front\Auth\LoginRequest;
use App\Http\Requests\Front\Auth\RegisterRequest;
use App\Http\Requests\Front\Auth\VerifyCodeRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Services\Helpers\ResponseData;
use App\Services\User\VerifyCodeService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public $loginAfterSignUp = true;

    public function register(RegisterRequest $request)
    {
        $user = User::create($request->validated());
        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return ResponseData::error('مشخصات کاربر اشتباه است لطفا دوباره تلاش کنید.', null, 401);
        }

        return $this->respondWithToken($token);
    }

    public function getAuthUser(Request $request)
    {
        return response()->json(auth()->user());
    }

    public function logout()
    {
        auth()->logout();
        return ResponseData::success('خروج با موفقیت انجام شد.');
    }

    protected function respondWithToken($token)
    {
        $data = [
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => auth()->factory()->getTTL()
        ];
        return ResponseData::success('', $data);
    }
}
