<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Food;
use App\Services\Helpers\ResponseData;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        $foods = Food::query()
            ->get()->filter(function ($value) {
            $ingredients = $value->ingredients();
            if ($ingredients->count() == $ingredients->where('stock', '>', 0)->where('expires_at', '>', date(Carbon::now()))->count())
                return true;
        });

        return ResponseData::success('', $foods);
    }
}
