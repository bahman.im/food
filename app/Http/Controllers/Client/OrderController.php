<?php

namespace App\Http\Controllers\Client;

use App\Events\DecreaseIngredientEvent;
use App\Http\Controllers\Controller;
use App\Models\Food;
use App\Services\Helpers\ResponseData;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        //Validation
        $this->validate($request, [
            'food_id' => 'required|exists:foods,id'
        ]);

        $food = Food::query()->findOrFail($request->food_id);
        if ($food->ingredients()->where('stock', '<=', 0)->exists()){
            return ResponseData::error('موجودی مواد اولیه این غدا به اتمام رسیده است.');
        }

        $user = auth()->user();

        $order = $user->orders()->create(['payment_driver' => 'zarinpal']);
        $order->foods()->attach($request->food_id);

        event(new DecreaseIngredientEvent($order));

        return ResponseData::success('سفارش با موفقیت انجام شد.', $order);
    }
}
