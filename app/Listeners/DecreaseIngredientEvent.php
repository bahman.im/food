<?php

namespace App\Listeners;

use App\Models\Order;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DecreaseIngredientEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(\App\Events\DecreaseIngredientEvent $event)
    {
        $ingredients = $event->order->food()->ingredients;

        foreach ($ingredients as $ingredient){
            $ingredient->update([
                'stock' => --$ingredient->stock
            ]);
        }
    }
}
