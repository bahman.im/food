<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Client\MenuController;
use App\Http\Controllers\Client\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class,'register'])->name('register');
Route::post('/login', [AuthController::class,'login'])->name('login');
Route::get('/logout', [AuthController::class,'logout'])->name('logout');
Route::get('/user', [AuthController::class,'getAuthUser'])->middleware(['api', 'auth']);

Route::middleware('auth')->prefix('/')->group(function () {
    Route::get('menu', [MenuController::class, 'index']);
    Route::post('orders', [OrderController::class, 'store']);
});
